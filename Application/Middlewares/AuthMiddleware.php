<?php
/**
 * Created by PhpStorm.
 * User: Amir
 * Date: 7/24/2018
 * Time: 2:06 PM
 */

namespace Application\Middlewares;

use Application\Exceptions\AuthException;
use Application\Middlewares\Contracts\Middleware;

class AuthMiddleware extends Middleware
{

    protected function handle()
    {
        if(!$this->isUserLoggedIn()){
//            throw new AuthException("Not loggedIn !");
            echo " User Not Logged In ";
            exit;
        }

    }

    private function isUserLoggedIn()
    {
        return isset($_SESSION['userID']) && intval($_SESSION['userID']) > 0;
    }
}